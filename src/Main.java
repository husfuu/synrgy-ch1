import java.util.Scanner;
import java.lang.Math;

public class Main {
    public static void main(String[] args) {
        menuUtama("");
    }
    // Declaring the background color
    public static final String ANSI_RED_BACKGROUND = "\u001B[41m";
    public static final String ANSI_GREEN_BACKGROUND = "\u001B[42m";
    public static final String ANSI_BLACK_TEXT = "\u001B[30m";
    public static final String ANSI_RESET = "\u001B[0m";

    public static void menuUtama(String peringatan){
        clearConsole();
        Scanner input = new Scanner(System.in);
        System.out.println(peringatan);
        System.out.println("-------------------------------------");
        System.out.println("Kalkulator Penghitung Luas dan Volume");
        System.out.println("-------------------------------------");
        System.out.println("Menu");
        System.out.println("1. Hitung luas bidang");
        System.out.println("2. Hitung volume ruang");
        System.out.println("0. Tutup aplikasi");
        System.out.println("-------------------------------------");
        System.out.print("Pilih: ");
        int option = input.nextInt();
        switch (option){
            case 1:
                System.out.println("ini menu luas bidang");
                menuLuasBidang();
                break;
            case 2:
                menuVolumeRuang();
                break;
            case 0:
                input.close();
                break;
            default:
                peringatan = ANSI_RED_BACKGROUND + "input dengan benar" + ANSI_RESET;
                menuUtama(peringatan);
        }
    }

    public static void menuLuasBidang(){
        clearConsole();
        String name, peringatan;
        double hasil;
        Scanner input = new Scanner(System.in);
        System.out.println("-------------------------------------");
        System.out.println("Pilih bidang yang akan dihitung");
        System.out.println("-------------------------------------");
        System.out.println("1. Persegi");
        System.out.println("2. Lingkaran");
        System.out.println("3. Segitiga");
        System.out.println("4. Persegi Panjang");
        System.out.println("0. Kembali ke menu sebelumnya");
        System.out.println("-------------------------------------");
        System.out.print("Pilih: ");
        int option = input.nextInt();

        switch (option){
            case 1:
                name = "Persegi";
                printPilihBidangAtauRuang(name);
                System.out.print("Masukan sisi: ");
                double sisi = input.nextDouble();
                hasil = hitungLuasPersegi(sisi);
                printHasil(name, hasil);
                break;
            case 2:
                name = "Lingkaran";
                printPilihBidangAtauRuang(name);
                System.out.print("Masukan jari-jari: ");
                double radius = input.nextDouble();
                hasil = hitungLuasLingkaran(radius);
                printHasil(name, hasil);
                break;
            case 3:
                name = "Segitiga";
                printPilihBidangAtauRuang(name);
                System.out.print("Masukan alas: ");
                double alas = input.nextDouble();
                System.out.print("Masukan tinggi: ");
                double tinggi = input.nextDouble();
                hasil = hitungLuasSegitiga(alas, tinggi);
                printHasil(name, hasil);
                break;
            case 4:
                name = "Persegi Panjang";
                printPilihBidangAtauRuang(name);
                System.out.print("Masukan panjang: ");
                double panjang = input.nextDouble();
                System.out.print("Masukan lebar: ");
                double lebar = input.nextDouble();
                hasil = hitungLuasPersegiPanjang(panjang, lebar);
                printHasil(name, hasil);
                break;
            case 0:
                menuUtama("");
                break;
            default:
                peringatan = ANSI_RED_BACKGROUND + "input dengan benar" + ANSI_RESET;
                menuUtama(peringatan);
        }
    }
    public static void menuVolumeRuang(){
        clearConsole();
        String name, peringatan;
        double sisi, panjang, tinggi, lebar, radius, hasil;
        Scanner input = new Scanner(System.in);
        System.out.println("-------------------------------------");
        System.out.println("Pilih bangun ruang yang akan dihitung");
        System.out.println("-------------------------------------");
        System.out.println("1. Kubus");
        System.out.println("2. Balok");
        System.out.println("3. Tabung");
        System.out.println("0. Kembali ke menu sebelumnya");
        System.out.println("-------------------------------------");
        System.out.print("Pilih: ");
        int option = input.nextInt();

        switch (option){
            case 1:
                name = "Kubus";
                printPilihBidangAtauRuang(name);
                System.out.print("Masukan sisi: ");
                sisi = input.nextDouble();
                hasil = hitungVolumeKubus(sisi);
                printHasil(name, hasil);
                break;
            case 2:
                name = "Balok";
                printPilihBidangAtauRuang(name);
                System.out.print("Masukan panjang: ");
                panjang = input.nextDouble();
                System.out.print("Masukan lebar: ");
                lebar = input.nextDouble();
                System.out.print("Masukan tinggi: ");
                tinggi = input.nextDouble();
                hasil = hitungVolumeBalok(panjang, lebar, tinggi);
                printHasil(name, hasil);
                break;
            case 3:
                name = "Tabung";
                printPilihBidangAtauRuang(name);
                System.out.print("Masukan tinggi: ");
                tinggi = input.nextDouble();
                System.out.print("Masukan jari-jari: ");
                radius = input.nextDouble();
                hasil = hitungVolumeTabung(tinggi, radius);
                printHasil(name, hasil);
                break;
            case 0:
                menuUtama("");
                break;
            default:
                peringatan = ANSI_RED_BACKGROUND + "input dengan benar" + ANSI_RESET;
                menuUtama(peringatan);
                break;
        }
    }
    public static double hitungLuasPersegi(double sisi){
        double hasil = sisi * sisi;
        return hasil;
    }
    public static double hitungLuasPersegiPanjang(double panjang, double lebar){
        double hasil = panjang * lebar;
        return hasil;
    }
    public static double hitungLuasLingkaran(double radius){
        double hasil = Math.PI * Math.pow(radius, 2);
        return hasil;
    }
    public static double hitungLuasSegitiga(double alas, double tinggi){
        double hasil = Math.PI * alas * tinggi;
        return hasil;
    }

    public static double hitungVolumeKubus(double sisi){
        double hasil = sisi * sisi * sisi;
        return hasil;
    }
    public static double hitungVolumeBalok(double panjang, double lebar, double tinggi){
        double hasil = panjang * lebar * tinggi;
        return hasil;
    }
    public static double hitungVolumeTabung(double tinggi, double radius){
        double hasil = Math.PI * tinggi * Math.pow(radius, 2);
        return hasil;
    }
    public static void printPilihBidangAtauRuang(String name){
        clearConsole();
        System.out.println("-------------------------------------");
        System.out.println("Anda memilih " + name);
        System.out.println("-------------------------------------");
    }
    public static void printHasil(String name, double hasil){
        Scanner input = new Scanner(System.in);
        System.out.println("processing...");
        System.out.println(ANSI_BLACK_TEXT + ANSI_GREEN_BACKGROUND + "Luas dari " + name + " adalah " + hasil + ANSI_RESET);
        System.out.println("-------------------------------------");
        System.out.println("Input apa saja untuk kembali ke menu utama");
        input.next();
        menuUtama("");
    }
    public static void clearConsole(){
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }
}