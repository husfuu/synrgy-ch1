# Kalkulator Luas dan Volume
Repository aplikasi kalkulator untuk menghitung luas pada beberapa bidang datar dan menghitung volume pada bidang ruang.
Daftar bidang datar dan ruang yang dapat disediakan oleh aplikasi ini:
* Persegi
* Persegi Panjang
* Lingkaran
* Segitiga
* Kubus
* Balok
* Tabung

## Tampilan
### Menu Awal
![alt text](./img/menu_awal.png "tampilan awal")

### Menu Luas Bidang
![alt text](./img/menu_luas_bidang.png "tampilan luas bidang")

### Peringatan
![alt text](./img/peringatan.png "tampilan luas bidang")

### Sukses
![alt text](./img/sukses.png "tampilan luas bidang")